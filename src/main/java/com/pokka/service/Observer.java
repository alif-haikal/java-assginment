package com.pokka.service;

public class Observer extends Thread {
	FileService fileService = new FileService();
	
	//async concept will do later..focus on flow process 1st
	public void run() {
		while (true) {
			try {
				
				//check csv directory is not empty
				if(!this.fileService.isDirEmpty()) {
					this.fileService.initProcess();
				}
					
				Thread.sleep(30000);
			} catch (Exception e) {

				System.out.println("Thread Error : " + e);
			}
		}
	}

}
