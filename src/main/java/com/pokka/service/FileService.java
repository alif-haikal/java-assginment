package com.pokka.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.pokka.core.DbContext;

public class FileService {
	File folder = new File("/home/haikal/eclipse-workspace/assignment/src/main/resources/csv/");

	public boolean isDirEmpty() throws Exception {
		int fileCount = this.folder.list().length;
		if (fileCount > 0) {
			return false;
		}
		return true;

	}

	//change to execProcess name
	public void initProcess() throws Exception {
		this.createTable();
//		this.insertData();
	}
	
	private void createTable() throws Exception {
		String filename, sql, headers = "";

		File[] listOfFiles = this.folder.listFiles();
		Connection con = DbContext.getConnection();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				filename = listOfFiles[i].getName();
				BufferedReader br = new BufferedReader(new FileReader(folder + "/"+ filename));
				headers = br.readLine();
			    String[] columnArr = headers.split(",");
				sql = "CREATE TABLE IF NOT EXISTS " + filename.replaceFirst("[.][^.]+$", "") + " (\n";
			
			    for(String column : columnArr) {
					sql = sql + column .substring(1 , column.length() - 1 ) + " VARCHAR(255) NOT NULL,";
			    }
			    //remove symbol (,) at last string sql
			    sql = sql .substring(0 , sql.length() - 1 );
				sql = sql + ")  ENGINE=INNODB;";

				PreparedStatement ps = con.prepareStatement(sql);
				ps.execute();

			}
		}
//		return true;
	}

	private void  insertData() throws Exception {
		String filename, sql, headers = "";

		File[] listOfFiles = this.folder.listFiles();
		Connection con = DbContext.getConnection();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				// create table
				filename = listOfFiles[i].getName();
				sql = "CREATE TABLE IF NOT EXISTS " + filename.replaceFirst("[.][^.]+$", "") + " (\n";
				BufferedReader br = new BufferedReader(new FileReader(folder + "/"+ filename));
				headers = br.readLine();
				
				//find data start from secline and do insert
//				while ((line = br.readLine()) != null) {
//					System.out.println(line);
//				}
				sql = sql + "    task_id INT AUTO_INCREMENT PRIMARY KEY,\n";
				sql = sql + "    title VARCHAR(255) NOT NULL,\n";
				sql = sql + "    start_date DATE,\n";
				sql = sql + "    due_date DATE,\n";
				sql = sql + "    status TINYINT NOT NULL,\n";
				sql = sql + "    priority TINYINT NOT NULL,\n";
				sql = sql + "    description TEXT,\n";
				sql = sql + "    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP\n";
				sql = sql + ")  ENGINE=INNODB;";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.execute();

			}
		}
//		return true;
	}
}
