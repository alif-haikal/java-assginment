package com.pokka.core;

import java.sql.Connection;
import java.sql.DriverManager;
 
public class DbContext  {
    
    static Connection con;
    static String driver = "com.mysql.cj.jdbc.Driver";
    static String url = "jdbc:mysql://localhost/assignment_java";
    static String uname = "root";
    static String pass = "Admin@123";
   
    
    public static Connection getConnection() throws Exception{
        if(con == null){
            Class.forName(driver);
            con = DriverManager.getConnection(url,uname, pass);
        }
        return con;
    }
 
}