package com.pokka.main;

import com.pokka.service.Observer;

public class Main {
	public static void main(String[] args) {
		try {
			Observer thread1 = new Observer();
			thread1.setDaemon(true);
			thread1.start();
			thread1.join();
			
			System.out.println(thread1.isDaemon());
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
		}

	}

}
